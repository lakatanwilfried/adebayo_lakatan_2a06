package com.adebayo.anywrrecruitmenttest.service;

import com.adebayo.anywrrecruitmenttest.model.Student;
import org.springframework.data.domain.Page;

public interface StudentService {
    Page<Student> getStudentByClassNameAndOrTeacherFullName(String className, String teacherFirstName, String teacherLastname);
    Page<Student> getAllStudent(int page, int size, String search);
}
