package com.adebayo.anywrrecruitmenttest.service;

import com.adebayo.anywrrecruitmenttest.dao.StudentRepository;
import com.adebayo.anywrrecruitmenttest.model.Student;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

@Service
public class StudentServiceImpl implements StudentService {

    @Autowired
    private StudentRepository studentRepository;

    @Override
    public Page<Student> getStudentByClassNameAndOrTeacherFullName(String className, String teacherFirstName, String teacherLastname) {
        return null;
    }

    @Override
    public Page<Student> getAllStudent(int page,
                                       int size,
                                       String search) {
        if(search.isEmpty()){
            return studentRepository.findAll(PageRequest.of(page, size));
        }else {
            return studentRepository.findByClassroom_NameLikeOrClassroom_Teacher_FirstNameLikeOrClassroom_Teacher_LasteNameLike(search, search, search, PageRequest.of(page, size));
        }
    }
}
