package com.adebayo.anywrrecruitmenttest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AnywrrecruitmenttestApplication {

	public static void main(String[] args) {
		SpringApplication.run(AnywrrecruitmenttestApplication.class, args);
	}

}
