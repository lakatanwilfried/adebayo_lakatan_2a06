package com.adebayo.anywrrecruitmenttest.controller;

import com.adebayo.anywrrecruitmenttest.model.Student;
import com.adebayo.anywrrecruitmenttest.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/student")
public class StudentController {

    @Autowired
    private StudentService studentService;

    @GetMapping("/all")
    public Page<Student> getAllStudent(@RequestParam(defaultValue = "0", required = false) int page, @RequestParam(defaultValue = "20", required = false) int size, @RequestParam(defaultValue = "", required = false) String search) {
        return studentService.getAllStudent(page, size, search);
    }
}
