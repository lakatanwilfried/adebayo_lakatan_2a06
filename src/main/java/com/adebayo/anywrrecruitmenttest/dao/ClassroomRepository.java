package com.adebayo.anywrrecruitmenttest.dao;

import com.adebayo.anywrrecruitmenttest.model.Classroom;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ClassroomRepository extends JpaRepository<Classroom, Long> {
}
