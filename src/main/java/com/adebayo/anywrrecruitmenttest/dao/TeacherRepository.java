package com.adebayo.anywrrecruitmenttest.dao;

import com.adebayo.anywrrecruitmenttest.model.Teacher;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TeacherRepository extends JpaRepository<Teacher, Long> {
}
