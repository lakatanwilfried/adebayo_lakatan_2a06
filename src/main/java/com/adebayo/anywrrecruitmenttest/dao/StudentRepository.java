package com.adebayo.anywrrecruitmenttest.dao;

import com.adebayo.anywrrecruitmenttest.model.Student;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StudentRepository extends JpaRepository<Student, Long> {
    Page<Student> findByClassroom_NameLikeOrClassroom_Teacher_FirstNameLikeOrClassroom_Teacher_LasteNameLike(String name, String firstName, String lastName, Pageable pageable);
}
